import {
  injectable,
  IEventDispatcher,
  inject,
  EventDispatcher
} from '@robotlegsjs/core';
import { Status } from './vo/Status';
import { GameEvent } from '../events/GameEvent';

@injectable()
export class GameModel {
  public score: number;
  public lives: number;
  public gameSpeed: number;
  public status: String;
  public fruitsCatched: number;
  public pointsToAdd: number;
  @inject(IEventDispatcher) public eventDispatcher: IEventDispatcher;

  constructor() {
    this.clear();
  }

  clear(): void {
    this.lives = 10;
    this.score = 0;
    this.fruitsCatched = 0;
    this.pointsToAdd = 0;
    this.gameSpeed = 1.5;
  }

  setStatus(status: string) {
    this.status = status;
    (<EventDispatcher>this.eventDispatcher).dispatchEventWith(
      GameEvent.UPDATE_HUD_DATA
    );
  }

  decreaseLives() {
    this.lives -= 1;
    if (this.lives === 0) {
      (<EventDispatcher>this.eventDispatcher).dispatchEventWith(
        GameEvent.GAME_OVER
      );
    }
    (<EventDispatcher>this.eventDispatcher).dispatchEventWith(
      GameEvent.UPDATE_HUD_DATA
    );
  }

  addPoints(score: number) {
    this.pointsToAdd = score;
    this.score += this.pointsToAdd;
    this.fruitsCatched++;

    if (this.fruitsCatched % 10 === 0) {
      this.gameSpeed += 0.5;
      console.log('Speed changed: ', this.gameSpeed);
    }
    (<EventDispatcher>this.eventDispatcher).dispatchEventWith(
      GameEvent.UPDATE_HUD_DATA
    );
  }
}
