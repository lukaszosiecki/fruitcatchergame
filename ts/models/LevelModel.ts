import { injectable, IEventDispatcher, inject } from '@robotlegsjs/core';
import { Entity } from '../view/Entity';
import { SpriteKeys } from './vo/SpriteKeys';
import { GameView } from '../view/GameView';
import { Stage } from './vo/Stage';

@injectable()
export class LevelModel {
  private _character: Entity;
  private _gameView: GameView;
  private _fruitsToAdd: Entity[] = [];
  private _fruitsAdded: Entity[] = [];
  @inject(IEventDispatcher) public eventDispatcher: IEventDispatcher;

  public get fruitsToAdd(): Entity[] {
    return this._fruitsToAdd;
  }

  public addFruit() {
    if (this.fruitsToAdd.length > 0) {
      let fruitToAdd = this.fruitsToAdd[0];
      this.fruitsAdded.push(fruitToAdd);
      this._gameView.addChild(fruitToAdd.display);

      fruitToAdd.x =
        Stage.FRUIT_POSITIONS[
          Math.floor(Math.random() * Stage.FRUIT_POSITIONS.length)
        ];
      fruitToAdd.y = -20;
      fruitToAdd.applyPosition();

      this.fruitsToAdd.shift();
    }
  }

  public removeFruit(fruit: Entity) {
    this.fruitsToAdd.push(fruit);
    this._gameView.removeChild(fruit.display);
    for (let i = 0; i < this.fruitsAdded.length; i++) {
      const fruitToCheck = this.fruitsAdded[i];
      if (fruitToCheck.id == fruit.id) {
        this.fruitsAdded.splice(i, 1);
        break;
      }
    }
  }

  public get fruitsAdded(): Entity[] {
    return this._fruitsAdded;
  }

  public get character(): Entity {
    return this._character;
  }

  public clear(): void {
    this._character = new Entity();
    this._fruitsToAdd = new Array<Entity>();
    this._fruitsAdded = new Array<Entity>();
  }

  public setCharacter(entity: Entity): void {
    this._character = entity;
    this._character.y = 500;
  }

  public setGameView(view: GameView) {
    this._gameView = view;
  }

  public get gameView(): GameView {
    return this._gameView;
  }

  public setFruits() {
    for (let i = 0; i < 24; i++) {
      let fruit = new Entity();
      fruit.id = i;
      fruit.score = 5 + i * 5;
      fruit.display = new PIXI.Sprite(
        SpriteKeys.getTexture(SpriteKeys.FRUITS[i % 8])
      );
      this.fruitsToAdd.push(fruit);
    }
  }

  private removeFromList(entity: Entity, list: Entity[]): void {
    const index = list.indexOf(entity);
    if (index > -1) {
      list.splice(index, 1);
    }
  }
}
