export class Stage {
  public static STAGE_WIDTH: number = 320;
  public static STAGE_HEIGHT: number = 640;
  public static FRUIT_POSITIONS: number[] = [40, 80, 120, 160, 200, 240];
}
