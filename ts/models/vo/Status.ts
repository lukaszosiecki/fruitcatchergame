export class Status {
  public static GAME = 'game';
  public static GAME_OVER = 'gameOver';
  public static MENU = 'menu';
}
