export class Labels {
  public static FONT_SIZE_DEFAULT = 22;
  public static FONT_SIZE_HUD = 14;

  public static GAME_OVER = 'GAME OVER';
  public static GAME_OVER_CLICK = 'GAME OVER \nCLICK TO PLAY AGAIN';
  public static SCORE = 'SCORE:';

  public static COMMANDS = 'USE THE ARROWS TO MOVE';
}
