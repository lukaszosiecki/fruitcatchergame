import { Texture, utils } from 'pixi.js';

export class SpriteKeys {
  public static FRUITS = [
    'Apple.png',
    'MelonCantaloupe.png',
    'MelonHoneydew.png',
    'MelonWater.png',
    'Onion.png',
    'Peach.png',
    'Strawberry.png',
    'Tomato.png'
  ];

  public static CHARACTER_LEFT = 'knight iso char_run left_';
  public static CHARACTER_RIGHT = 'knight iso char_run right_';
  public static CHARACTER_IDLE = 'knight iso char_idle_';

  // public static SPXML = "./assets/spaceinvaders-pixijs-atlas.json";
  // public static SPPNG = "./assets/spaceinvaders-pixijs-atlas.png";

  public static getTexture(atlasKey: string): Texture {
    return utils.TextureCache[atlasKey];
  }
}
