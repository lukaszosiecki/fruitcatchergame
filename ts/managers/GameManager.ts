import { inject, injectable } from '@robotlegsjs/core';

import { GameModel } from '../models/GameModel';
import { LevelModel } from '../models/LevelModel';
import { GameService } from '../services/GameService';
import { Stage } from '../models/vo/Stage';
import { Rectangle } from '../utils/Rectangle';
import { Status } from '../models/vo/Status';

@injectable()
export class GameManager {
  @inject(GameModel) private gameModel: GameModel;
  @inject(LevelModel) private levelModel: LevelModel;
  @inject(GameService) private service: GameService;

  private _characterDirection: number = 0;

  constructor() {}

  init() {
    setTimeout(() => this.addFruit(), 1500);
  }

  addFruit() {
    if (!document.hidden) {
      this.levelModel.addFruit();
    } else {
      console.log('window inactive');
    }
    if (this.gameModel.status !== Status.GAME_OVER) {
      setTimeout(() => this.addFruit(), 3000 / this.gameModel.gameSpeed);
    }
  }
  public characterMovement(direction = 0): void {
    this._characterDirection = direction;
  }
  public update(): void {
    this.moveCharacter();
    this.moveFruits();
    this.checkCollisions();
  }

  private moveFruits(): void {
    for (const fruit of this.levelModel.fruitsAdded) {
      fruit.y += this.gameModel.gameSpeed;
      if (fruit.y > Stage.STAGE_HEIGHT) {
        this.levelModel.removeFruit(fruit);
        this.gameModel.decreaseLives();
        // this.service.decreaseLives();
      }
      fruit.applyPosition();
    }
  }
  private checkCollisions(): void {
    for (const fruit of this.levelModel.fruitsAdded) {
      const fruitRect: Rectangle = new Rectangle(fruit.x, fruit.y, 16, 16);
      const characterRect: Rectangle = new Rectangle(
        this.levelModel.character.x + 22,
        this.levelModel.character.y + 8,
        40,
        40
      );
      if (fruitRect.intersects(characterRect)) {
        this.levelModel.removeFruit(fruit);

        this.gameModel.addPoints(fruit.score);
      }
      break;
    }
  }

  private moveCharacter(): void {
    let newCharacterXPosition: number =
      this.levelModel.character.x + this._characterDirection;
    newCharacterXPosition = Math.min(
      Stage.STAGE_WIDTH - 62,
      newCharacterXPosition
    );
    newCharacterXPosition = Math.max(-22, newCharacterXPosition);
    this.levelModel.character.x = newCharacterXPosition;
    this.levelModel.character.applyPosition();
  }
}
