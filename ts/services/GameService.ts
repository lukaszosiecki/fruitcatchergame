import {
  EventDispatcher,
  IEventDispatcher,
  inject,
  injectable
} from '@robotlegsjs/core';
import { GameEvent } from '../events/GameEvent';

@injectable()
export class GameService {
  @inject(IEventDispatcher) public eventDispatcher: IEventDispatcher;

  public assetsLoaded(): void {
    this.dispatchEventWith(GameEvent.ASSETS_LOADED);
  }
  public createLevelCommand(): void {
    this.dispatchEventWith(GameEvent.CREATE_LEVEL_COMMAND);
  }
  public increaseLevel(): void {
    this.dispatchEventWith(GameEvent.INCREASE_LEVEL_COMMAND);
  }
  public startCommand(): void {
    this.dispatchEventWith(GameEvent.START_GAME_COMMAND);
  }
  public dispatchEventWith(type: string): void {
    (<EventDispatcher>this.eventDispatcher).dispatchEventWith(type);
  }
}
