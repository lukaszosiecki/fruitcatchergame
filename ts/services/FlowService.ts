import {
  EventDispatcher,
  IEventDispatcher,
  inject,
  injectable
} from '@robotlegsjs/core';
import { PalidorEvent } from '@robotlegsjs/pixi-palidor';

import { FlowEvent } from '../events/FlowEvent';

@injectable()
export class FlowService {
  @inject(IEventDispatcher) public eventDispatcher: IEventDispatcher;

  public setGameView(): void {
    this.dispatchEventWith(FlowEvent.SHOW_GAME_VIEW);
  }
  public setMenuView(): void {
    this.dispatchEventWith(FlowEvent.SHOW_MENU_VIEW);
  }
  public dispatchEventWith(type: string): void {
    (<EventDispatcher>this.eventDispatcher).dispatchEventWith(type);
  }
}
