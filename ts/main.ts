import 'reflect-metadata';

import PIXI = require('pixi.js');

import { Context, MVCSBundle } from '@robotlegsjs/core';
import { ContextView, PixiBundle } from '@robotlegsjs/pixi';
import { PalidorPixiExtension } from '@robotlegsjs/pixi-palidor';

import { GameConfig } from './config/GameConfig';
import { Stage } from './models/vo/Stage';

class Main {
  private stage: PIXI.Container;
  private renderer: PIXI.CanvasRenderer | PIXI.WebGLRenderer;
  private context: Context;

  constructor() {
    this.renderer = PIXI.autoDetectRenderer(
      Stage.STAGE_WIDTH,
      Stage.STAGE_HEIGHT,
      {}
    );
    this.stage = new PIXI.Container();

    this.context = new Context();
    this.context
      .install(MVCSBundle, PixiBundle, PalidorPixiExtension)
      .configure(new ContextView(this.stage))
      .configure(GameConfig)
      .initialize();

    document.body.appendChild(this.renderer.view);
  }

  public render = () => {
    this.renderer.render(this.stage);
    window.requestAnimationFrame(this.render);
  };
}

let main = new Main();
main.render();
