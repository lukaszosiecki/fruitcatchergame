import { inject, injectable } from '@robotlegsjs/core';
import { Mediator } from '@robotlegsjs/pixi';
import { LoadingView } from './LoadingView';
import { GameService } from '../services/GameService';
import { FlowService } from '../services/FlowService';
import { SpriteKeys } from '../models/vo/SpriteKeys';

@injectable()
export class LoadingViewMediator extends Mediator<LoadingView> {
  @inject(GameService)
  private gameService: GameService;

  @inject(FlowService)
  private flowService: FlowService;

  public initialize(): void {
    PIXI.loader.add('assets', 'assets/sprites.json').load(this.onLoaded);
  }
  onLoaded = () => {
    this.gameService.assetsLoaded();
    setTimeout(() => {
      this.flowService.setGameView();
    }, 500);
  };

  public destroy(): void {
    console.log('LoadingViewMediator destroyed!');
  }
}
