import { Container, Text } from 'pixi.js';
import { GameModel } from '../../models/GameModel';
import { Colors } from '../../models/vo/Colors';
import { Helpers } from '../../utils/Helpers';
import { Labels } from '../../models/vo/Labels';
import { Stage } from '../../models/vo/Stage';
import * as TWEEN from '@tweenjs/tween.js';

export class HUDComponent extends Container {
  private livesText: Text;
  private scoreText: Text;
  private currentScore: number = 0;

  public constructor() {
    super();

    this.createTextFields();
  }

  public updateData(model: GameModel): void {
    if (model.score === 0) {
      this.scoreText.text = 'score: ' + String(model.score);
    }
    this.livesText.text = 'lives: ' + String(model.lives);

    if (this.currentScore != model.score) {
      let val = { score: this.currentScore };
      this.currentScore = model.score;
      let tween = new TWEEN.Tween(val)
        .to(
          {
            score: model.score
          },
          1000
        )
        .easing(TWEEN.Easing.Sinusoidal.InOut)
        .onUpdate(e => {
          this.scoreText.text = 'score: ' + String(Math.trunc(val.score));
        })
        .start();
    }
  }

  private createTextFields(): void {
    this.scoreText = Helpers.getTextField(
      '',
      Colors.DYNAMIC_TEXT,
      Labels.FONT_SIZE_HUD
    );
    this.scoreText.x = 16;
    this.scoreText.y = 16;
    this.scoreText.anchor.x = 0;
    this.addChild(this.scoreText);

    this.livesText = Helpers.getTextField(
      '',
      Colors.DYNAMIC_TEXT,
      Labels.FONT_SIZE_HUD
    );
    this.livesText.x = Stage.STAGE_WIDTH - 16;
    this.livesText.y = 16;
    this.livesText.anchor.x = 1;
    this.addChild(this.livesText);
  }
}
