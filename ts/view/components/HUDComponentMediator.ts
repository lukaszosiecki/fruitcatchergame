import { inject, injectable } from '@robotlegsjs/core';
import { Mediator } from '@robotlegsjs/pixi';
import { GameEvent } from '../../events/GameEvent';
import { GameModel } from '../../models/GameModel';
import { FlowService } from '../../services/FlowService';
import { GameService } from '../../services/GameService';
import { HUDComponent } from './HUDComponent';

@injectable()
export class HUDComponentMediator extends Mediator<HUDComponent> {
  @inject(GameModel) private model: GameModel;
  @inject(GameService) private gameService: GameService;

  public initialize(): void {
    this.eventMap.mapListener(
      this.eventDispatcher,
      GameEvent.UPDATE_HUD_DATA,
      this.onHUDUpdate,
      this
    );
  }
  public destroy(): void {
    this.eventMap.unmapListeners();
  }
  private onHUDUpdate(e: any): void {
    this.view.updateData(this.model);
  }
}
