import { Container, extras, Sprite } from 'pixi.js';
import { SpriteKeys } from '../../models/vo/SpriteKeys';
import { Helpers } from '../../utils/Helpers';

export class CharacterComponent extends Sprite {
  leftRunAnimation: extras.AnimatedSprite;
  rightRunAnimation: extras.AnimatedSprite;
  idleAnimation: extras.AnimatedSprite;
  animations: extras.AnimatedSprite[];

  constructor() {
    super();
    this.idleAnimation = new extras.AnimatedSprite(
      Helpers.getSpriteFrames(SpriteKeys.CHARACTER_IDLE, 3)
    );
    this.rightRunAnimation = new extras.AnimatedSprite(
      Helpers.getSpriteFrames(SpriteKeys.CHARACTER_RIGHT, 5)
    );
    this.leftRunAnimation = new extras.AnimatedSprite(
      Helpers.getSpriteFrames(SpriteKeys.CHARACTER_LEFT, 5)
    );
    this.animations = [
      this.leftRunAnimation,
      this.rightRunAnimation,
      this.idleAnimation
    ];

    for (let i = 0; i < this.animations.length; i++) {
      const e = this.animations[i];
      e.animationSpeed = 0.2;
      e.loop = true;
      this.addChild(e);
    }
    this.playIdle();
  }

  resetAnimations() {
    for (let i = 0; i < this.animations.length; i++) {
      const e = this.animations[i];
      e.stop();
      e.visible = false;
    }
  }
  playAnimation(animation: extras.AnimatedSprite) {
    animation.visible = true;
    animation.play();
  }

  playLeft() {
    this.resetAnimations();
    this.playAnimation(this.leftRunAnimation);
  }

  playRight() {
    this.resetAnimations();
    this.playAnimation(this.rightRunAnimation);
  }

  playIdle() {
    this.resetAnimations();
    this.playAnimation(this.idleAnimation);
  }
}
