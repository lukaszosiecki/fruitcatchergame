import { Container, Text } from 'pixi.js';
import { Helpers } from '../utils/Helpers';
import { Stage } from '../models/vo/Stage';

export class LoadingView extends Container {
  constructor() {
    super();

    let label: Text = Helpers.getTextField('Loading...');
    label.anchor.x = 0.5;
    label.x = Stage.STAGE_WIDTH * 0.5;
    label.y = 200;
    this.addChild(label);
  }
}
