import { Container, Text } from 'pixi.js';
import { Helpers } from '../utils/Helpers';
import { Stage } from '../models/vo/Stage';
import { Labels } from '../models/vo/Labels';

export class MenuView extends Container {
  constructor() {
    super();

    let label: Text = Helpers.getTextField(Labels.GAME_OVER_CLICK);
    label.anchor.x = 0.5;
    label.x = Stage.STAGE_WIDTH * 0.5;
    label.y = 200;
    this.addChild(label);
  }
}
