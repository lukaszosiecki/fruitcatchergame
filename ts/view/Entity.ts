import { Sprite } from 'pixi.js';

export class Entity {
  public x: number = 0;
  public y: number = 0;

  public display: Sprite;
  public id: number = 0;
  public score: number = 0;

  public applyPosition(): void {
    this.display.x = this.x;
    this.display.y = this.y;
  }
}
