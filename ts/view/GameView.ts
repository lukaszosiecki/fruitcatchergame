import { Container } from 'pixi.js';
import { CharacterComponent } from './components/CharacterComponent';
import { HUDComponent } from './components/HUDComponent';

export class GameView extends Container {
  private _character: CharacterComponent;
  private hud: HUDComponent;

  constructor() {
    super();
  }
  public destroy(): void {
    this.removeChild(this._character);
    this.removeChild(this.hud);

    this._character = null;
    this.hud = null;
  }

  public get character(): CharacterComponent {
    return this._character;
  }

  public createComponents(): void {
    this._character = new CharacterComponent();
    this.addChild(this._character);

    this.hud = new HUDComponent();
    this.addChild(this.hud);
  }
}
