import { MenuView } from './MenuView';
import { Mediator } from '@robotlegsjs/pixi';
import { FlowService } from '../services/FlowService';
import { inject } from '@robotlegsjs/core';

export class MenuViewMediator extends Mediator<MenuView> {
  @inject(FlowService) private flowService: FlowService;

  public initialize(): void {
    console.log('Menu initialized!');
    this.view.interactive = true;
    this.addViewListener('click', this.onClick, this);
  }

  public onClick(e: any): void {
    this.flowService.setGameView();
  }

  public destroy(): void {}
}
