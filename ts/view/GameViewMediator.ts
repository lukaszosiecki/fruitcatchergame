import { GameService } from '../services/GameService';
import { GameManager } from '../managers/GameManager';
import { GameView } from './GameView';
import { Mediator } from '@robotlegsjs/pixi';
import { inject, injectable } from '@robotlegsjs/core';
import { LevelModel } from '../models/LevelModel';
import { GameEvent } from '../events/GameEvent';
import { Entity } from './Entity';
import { GameModel } from '../models/GameModel';
import { Status } from '../models/vo/Status';
import * as TWEEN from '@tweenjs/tween.js';

@injectable()
export class GameViewMediator extends Mediator<GameView> {
  @inject(GameService) private gameService: GameService;
  @inject(GameManager) private gameManager: GameManager;
  @inject(LevelModel) private levelModel: LevelModel;
  @inject(GameModel) private gameModel: GameModel;

  public initialize(): void {
    this.view.createComponents();
    console.log('mediator init:');

    document.addEventListener('keydown', this.onKeyDownMovement);
    document.addEventListener('keyup', this.onKeyUpMovement);

    this.levelModel.setGameView(this.view);
    this.levelModel.setCharacter(new Entity());
    this.levelModel.character.display = this.view.character;
    this.levelModel.setFruits();

    this.gameService.startCommand();
    this.gameManager.init();
    window.requestAnimationFrame(this.update);
  }

  public destroy(): void {
    document.removeEventListener('keydown', this.onKeyDownMovement);
    document.removeEventListener('keyup', this.onKeyUpMovement);

    this.gameModel.clear();
    this.levelModel.clear();

    this.view.destroy();
  }

  private onKeyDownMovement = (e: KeyboardEvent) => {
    if (e.keyCode === 37 || e.keyCode === 65) {
      this.gameManager.characterMovement(-3);
      this.view.character.playLeft();
    } else if (e.keyCode === 39 || e.keyCode === 68) {
      this.gameManager.characterMovement(3);
      this.view.character.playRight();
    }
  };
  private onKeyUpMovement = (e: KeyboardEvent) => {
    if (
      e.keyCode === 37 ||
      e.keyCode === 65 ||
      e.keyCode === 39 ||
      e.keyCode === 68
    ) {
      this.view.character.playIdle();
      this.gameManager.characterMovement(0);
    }
  };

  private update = (e: any) => {
    TWEEN.update();
    if (this.gameModel.status !== Status.GAME_OVER) {
      this.gameManager.update();
      window.requestAnimationFrame(this.update);
    }
  };
}
