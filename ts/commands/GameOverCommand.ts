import { ICommand, inject, injectable } from '@robotlegsjs/core';

import { GameModel } from '../models/GameModel';
import { Status } from '../models/vo/Status';
import { FlowService } from '../services/FlowService';

@injectable()
export class GameOverCommand implements ICommand {
  @inject(GameModel) private gameModel: GameModel;
  @inject(FlowService) private flowService: FlowService;

  public execute(): void {
    this.gameModel.setStatus(Status.GAME_OVER);
    this.flowService.setMenuView();
  }
}
