import { ICommand, inject, injectable } from '@robotlegsjs/core';

import { GameModel } from '../models/GameModel';
import { FlowService } from '../services/FlowService';
import { GameService } from '../services/GameService';
import { Status } from '../models/vo/Status';

@injectable()
export class StartGameCommand implements ICommand {
  @inject(GameModel) private gameModel: GameModel;

  public execute(): void {
    this.gameModel.clear();
    this.gameModel.setStatus(Status.GAME);
  }
}
