import { ICommand, inject, injectable } from '@robotlegsjs/core';
import { GameModel } from '../models/GameModel';
import { Status } from '../models/vo/Status';

@injectable()
export class InitCommand implements ICommand {
  @inject(GameModel) private gameModel: GameModel;

  public execute(): void {
    this.gameModel.setStatus(Status.GAME);
  }
}
