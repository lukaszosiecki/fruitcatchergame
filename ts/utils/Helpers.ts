import { TextStyle, Text } from 'pixi.js';
import { Colors } from '../models/vo/Colors';
import { Labels } from '../models/vo/Labels';

export class Helpers {
  public static getTextField(
    text: string,
    color: number = Colors.DYNAMIC_TEXT,
    fontSize: number = Labels.FONT_SIZE_DEFAULT
  ): Text {
    const style = new TextStyle({
      align: 'center',
      fill: color,
      fontFamily: 'Arial',
      fontSize,
      fontWeight: 'bold'
    });
    return new Text(text, style);
  }

  public static getSpriteFrames(spriteName: string, count: number) {
    let frames: PIXI.Texture[] = [];
    for (var i = 0; i <= count; i++) {
      frames.push(PIXI.Texture.fromFrame(spriteName + i + '.png'));
    }
    return frames;
  }
}
