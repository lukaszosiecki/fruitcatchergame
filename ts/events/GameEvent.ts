export class GameEvent extends Event {
  public static START_GAME_COMMAND = 'startGameCommand';
  public static CREATE_LEVEL_COMMAND = 'createLevelCommand';
  public static INCREASE_LEVEL_COMMAND = 'increaseLevelCommand';
  public static GAME_OVER = 'gameOver';
  public static ASSETS_LOADED = 'assetsLoaded';
  public static RESUME = 'resume';
  public static PAUSE = 'pause';
  public static INCREASE_POINTS = 'increasePoints';
  public static DECREASE_LIVES = 'decreaseLives';
  public static UPDATE_HUD_DATA = 'updateData';

  constructor(type: string) {
    super(type);
  }
}
