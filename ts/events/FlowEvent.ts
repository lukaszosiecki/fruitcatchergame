import { Event } from '@robotlegsjs/core';

export class FlowEvent extends Event {
  public static SHOW_GAME_VIEW = 'showGameView';
  public static SHOW_MENU_VIEW = 'showMenuView';
  public static SHOW_LOADER_VIEW = 'showIntroView';
  public static SHOW_GAME_OVER_POPUP = 'showGameOverPopup';

  constructor(type: string) {
    super(type);
  }
}
