import {
  inject,
  injectable,
  IConfig,
  IContext,
  IEventCommandMap,
  IEventDispatcher
} from '@robotlegsjs/core';
import { IFlowManager } from '@robotlegsjs/pixi-palidor';
import { IMediatorMap } from '@robotlegsjs/pixi';

import { GameService } from '../services/GameService';
import { GameModel } from '../models/GameModel';
import { LevelModel } from '../models/LevelModel';
import { InitCommand } from '../commands/InitCommand';
import { GameEvent } from '../events/GameEvent';
import { FlowEvent } from '../events/FlowEvent';
import { FlowService } from '../services/FlowService';
import { LoadingView } from '../view/LoadingView';
import { LoadingViewMediator } from '../view/LoadingViewMediator';
import { MenuView } from '../view/MenuView';
import { MenuViewMediator } from '../view/MenuViewMediator';
import { GameView } from '../view/GameView';
import { GameViewMediator } from '../view/GameViewMediator';
import { HUDComponent } from '../view/components/HUDComponent';
import { HUDComponentMediator } from '../view/components/HUDComponentMediator';
import { GameManager } from '../managers/GameManager';
import { GameOverCommand } from '../commands/GameOverCommand';
import { StartGameCommand } from '../commands/StartGameCommand';

@injectable()
export class GameConfig implements IConfig {
  @inject(IMediatorMap)
  private mediatorMap: IMediatorMap;

  @inject(IEventCommandMap)
  private commandMap: IEventCommandMap;

  @inject(IContext)
  private context: IContext;

  @inject(IFlowManager)
  private flowManager: IFlowManager;

  @inject(IEventDispatcher)
  private eventDispatcher: IEventDispatcher;

  public configure(): void {
    this.context.injector
      .bind(GameManager)
      .to(GameManager)
      .inSingletonScope();

    this.commandMap.map(GameEvent.ASSETS_LOADED).toCommand(InitCommand);
    this.commandMap.map(GameEvent.GAME_OVER).toCommand(GameOverCommand);
    this.commandMap
      .map(GameEvent.START_GAME_COMMAND)
      .toCommand(StartGameCommand);

    this.mediatorMap.map(LoadingView).toMediator(LoadingViewMediator);
    this.mediatorMap.map(MenuView).toMediator(MenuViewMediator);
    this.mediatorMap.map(GameView).toMediator(GameViewMediator);
    this.mediatorMap.map(HUDComponent).toMediator(HUDComponentMediator);

    this.context.injector
      .bind(GameService)
      .to(GameService)
      .inSingletonScope();
    this.context.injector
      .bind(GameModel)
      .to(GameModel)
      .inSingletonScope();
    this.context.injector
      .bind(LevelModel)
      .to(LevelModel)
      .inSingletonScope();
    this.context.injector
      .bind(FlowService)
      .to(FlowService)
      .inSingletonScope();

    this.flowManager.map(FlowEvent.SHOW_LOADER_VIEW).toView(LoadingView);
    this.flowManager.map(FlowEvent.SHOW_MENU_VIEW).toView(MenuView);
    this.flowManager.map(FlowEvent.SHOW_GAME_VIEW).toView(GameView);

    this.eventDispatcher.dispatchEvent(
      new FlowEvent(FlowEvent.SHOW_LOADER_VIEW)
    );
  }
}
